﻿using System;
using project_ref;

namespace project_main
{
    class Program
    {
        static void Main(string[] args)
        {
            Class1 c1 = new Class1();
            var result = c1.Add(1, 1);
            
            Console.WriteLine(result);
        }
    }
}
