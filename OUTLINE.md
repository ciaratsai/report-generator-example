### Report Generator Proposal Outline ###

#### Problem / Motivation ####

The project is not be analysis.
Our project is not have a standard to check.

#### (How to resolve the problems?) What's Report Generator ####

OpenCover => C# Code coverage統計工具

#### Pros/Cons ####
1. open source
2. 

#### How to use in your projects? ####

最新版
方法一:
```bash
#直接用dotnet tool安裝global在電腦上
dotnet tool install -g 
dotnet-reportgenerator-globaltool
#要引入
<PackageReference Include="coverlet.msbuild" Version="3.1.2"/>
#安裝ReportGenerator在你的電腦，每次跑之前都要做一次
#using reportgenerator command line or build script
dotnet test /p:CollectCoverage=true /p:CoverletOutputFormat=opencover /p:CoverletOutput='./coverage.pencover

reportgenerator "-reports:coverage.opencover.xml" "-targetdir:./coverage-report" -reporttypes:HTML


Windows path: %USERPROFILE%\.dotnet\tools (EX: C:\Users\ciara.tsai\.dotnet\tools)
Linus/MacOS: $HOME/.dotnet/tools (EX: /home/ciara.tsai/.dotnet/tools)
```
方法二:
```bash
#要引入
<PackageReference Include="coverlet.msbuild" Version="3.1.2"/>
#安裝ReportGenerator在你的電腦，每次跑之前都要做一次
#using reportgenerator command line or build script
dotnet test /p:CollectCoverage=true /p:CoverletOutputFormat=opencover /p:CoverletOutput='./coverage.pencover

dotnet C:\Users\ciara.tsai\.nuget\packages\reportgenerator\5.1.4\tools\netcoreapp3.1\ReportGenerator.dll "-reports:coverage.opencover.xml" "-targetdir:coverage-report" -reporttypes:Html
 
Windows path: %USERPROFILE%\.nuget\packages\reportgenerator\5.1.4\tools\netcoreapp3.1\ReportGenerator.dll(
C:\Users\ciara.tsai\.nuget\packages\reportgenerator\5.1.4\tools\netcoreapp3.1\ReportGenerator.dll)

Linus/MacOS: $HOME/.nuget/tools (EX: /home/ciara.tsai/.nuget/tools)
```

執行要兩個步驟



- Step 1: Open test project file => `*.csproj`

- Step 2: Add a library.
  - <font color="red">coverlet.msbuild</font>

    ```xml
    <ItemGroup>
        <PackageReference Include="coverlet.msbuild" Version="3.1.2" />
    </ItemGroup>
    ```

- Step 3:

```bash
#
dotnet test /p:CollectCoverage=true /p:CoverletOutputFormat=opencover

#
dotnet reportgenerator "-reports:coverage.opencover.xml" "-targetdir:./coverage-report" -reporttypes:HTML

```

-- Step 4: Open your output `index.html`
![overall](./images/overall.PNG)

#### What's the numbers mean? ####
說明一下line coverage or branch coverage意義

#### Demo ####
1. Do a Add function and write the unit tests.
   how to descript the numbers?
2. 

#### Import report generator to `Higgs.Sportsbook.Vendor.Betradr.Processor.UnitTest`專案 ####


#### After sharing?  ####
1. 每個專案可以導入code coverage，在自己本機跑unit test就知道自己的程式碼測試涵蓋率
2. 可以在CI階段加入此unit test階段，加入tag也可以下載直接看code coverage 報告.
3. 記得將產生的`coverage.opencover.xml`(可自己命名)和`coverage-rport`資料夾做git ignore.


#### Reference ####
[1] [coverlet-coverage/coverlet](https://github.com/coverlet-coverage/coverlet)
[2] [Report-Generator](https://github.com/danielpalme/ReportGenerator)
[3] [Report-Generator-Usage](https://danielpalme.github.io/ReportGenerator/usage)
