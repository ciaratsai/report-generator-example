﻿// using project_ref2;

namespace project_ref
{
    public class Class1
    {

        public int Add(int? a, int? b)
        {
            if (a == null || b == null)
                return 0;
                
            return (int)a + (int)b;
        }

        // public int Add(int? a, int? b)
        // {
        //     if (a == null || b == null)
        //         return 0;

        //     Class2 c2 = new Class2();
                
        //     return c2.Add((int)a, (int)b);
        // }

        // public int Substract(int? a, int? b)
        // {
        //     if (a == null || b == null)
        //         return 0;
                
        //     return (int)a - (int)b;
        // }
    }
}
