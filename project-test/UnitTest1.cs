using project_ref;
using Xunit;

namespace project_test
{
    public class UnitTest1
    {
        [Theory]
        [InlineData(1, 1, 2)]
        [InlineData(null, null, 0)]
        // [InlineData(null, 1, 0)]
        public void TestAdd(int? a, int? b, int expectValue)
        {
            // Arrange

            // Act
            Class1 c1 = new Class1();
            var result = c1.Add(a, b);

            // Assert
            Assert.Equal(expectValue, result);
        }
    }
}
