# FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS dotnet-test-env
# RUN dotnet tool install --global coverlet.console && \
#     dotnet tool install --global dotnet-reportgenerator-globaltool
# ENV PATH=$PATH:/root/.dotnet/tools/
# WORKDIR /
# COPY . .
# RUN dotnet test \
#     /p:CollectCoverage=true \
#     /p:CoverletOutputFormat=opencover \
#     /p:CoverletOutput=/coverage/ \
#     UnitTest.sln
# RUN reportgenerator -reports:/coverage/coverage.opencover.xml -targetdir:/coverage

# FROM nginx:alpine
# RUN rm /usr/share/nginx/html/index.html
# COPY --from=dotnet-test-env /coverage /usr/share/nginx/html